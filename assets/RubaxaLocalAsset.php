<?php

namespace kotchuprik\sortable\assets;

use yii\web\AssetBundle;

class RubaxaLocalAsset extends AssetBundle
{
    public $sourcePath = '@npm/sortablejs';

    public $js = [
        'Sortable.min',
    ];
}
